/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/11 13:36:43 by triviere          #+#    #+#             */
/*   Updated: 2016/01/12 15:18:04 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "convert.hpp"

int					main(int ac, char **av)
{
	int				n = 0;
	float			f = 0;
	double			d = 0;
	char			c = 0;

	if (ac != 2)
	{
		std::cout << "usage : " << av[0] << " <char|int|float|double>" << std::endl;
		return (1);
	}

	if (!std::string(av[1]).compare("-inf") || !std::string(av[1]).compare("-inff")) {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: -inff" << std::endl;
		std::cout << "double: -inf" << std::endl;
		return (0);
	} else if (!std::string(av[1]).compare("+inf") || !std::string(av[1]).compare("+inff")) {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: +inff" << std::endl;
		std::cout << "double: +inf" << std::endl;
		return (0);
	}

	if (std::regex_match(av[1], std::regex("^-?[0-9]+$"))) {
		n = atoi(av[1]);
		f = static_cast<float>(n);
		d = static_cast<double>(n);
		c = static_cast<char>(n);
	} else if (std::regex_match(av[1], std::regex("^-?[0-9]+[.][0-9]*f$")) \
			|| !std::string(av[1]).compare("-inff") \
			|| !std::string(av[1]).compare("+inff") \
			|| !std::string(av[1]).compare("nanf")
	) {
		f = static_cast<float>(atof(av[1]));
		d = static_cast<double>(f);
		n = static_cast<int>(f);
		c = static_cast<char>(f);
	} else if (std::regex_match(av[1], std::regex("^-?[0-9]+[.][0-9]*$")) \
			|| !std::string(av[1]).compare("-inf") \
			|| !std::string(av[1]).compare("+inf") \
			|| !std::string(av[1]).compare("nan")
	) {
		d = static_cast<double>(atof(av[1]));
		f = static_cast<float>(d);
		n = static_cast<int>(d);
		c = static_cast<char>(d);
	} else if (std::regex_match(av[1], std::regex("^'.'$"))) {
		n = static_cast<int>(av[1][1]);
		f = static_cast<float>(n);
		d = static_cast<double>(n);
		c = static_cast<char>(n);
	} else {
		std::cout << "usage : " << av[0] << " <char|int|float|double>" << std::endl;
		return (1);
	}

	std::cout.setf(std::ios::fixed);
	if (std::isnan(d) || n > 127 || n < 0)
		std::cout << "char: impossible" << std::endl;
	else if (n < 32 || n > 126)
		std::cout << "char: Non displayable" << std::endl;
	else
		std::cout << "char: '" << c << "'" << std::endl;
	if (std::isnan(d))
		std::cout << "int: impossible" << std::endl;
	else
		std::cout << "int: " << n << std::endl;
	std::cout.precision(1);
	std::cout << "float: " << f << "f" << std::endl;
	std::cout << "double: " << d << std::endl;

	return (0);
}
