/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:46 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:47 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "identity.hpp"

int			main() {
	Base	*var = NULL;

	std::srand(std::time(0));
	var = generate();
	identify_from_pointer(var);
	identify_from_reference(*var);
	return (0);
}

Base		*generate(void) {
	switch(std::rand() % 3) {
		case (0) :
			return (new A());
		case (1) :
			return (new B());
		case (2) :
			return (new C());
	}
	return (NULL);
}

void		identify_from_pointer(Base *p) {
	A		*a = dynamic_cast<A *>(p);
	B		*b = dynamic_cast<B *>(p);
	C		*c = dynamic_cast<C *>(p);

	if (a)
		std::cout << "[PTR] : Instance of A" << std::endl;
	else if (b)
		std::cout << "[PTR] : Instance of B" << std::endl;
	else if (c)
		std::cout << "[PTR] : Instance of C" << std::endl;
	else
		std::cout << "[PTR] : Is not Instance of A, B or C" << std::endl;
}

void		identify_from_reference(Base &p) {
	try {
		A	a = dynamic_cast<A &>(p);
		std::cout << "[REF] : Instance of A" << std::endl;
		return ;
	} catch (std::bad_cast e) {}
	try {
		B	b = dynamic_cast<B &>(p);
		std::cout << "[REF] : Instance of B" << std::endl;
		return ;
	} catch (std::bad_cast e) {}
	try {
		C	c = dynamic_cast<C &>(p);
		std::cout << "[REF] : Instance of C" << std::endl;
		return ;
	} catch (std::bad_cast e) {}
}
