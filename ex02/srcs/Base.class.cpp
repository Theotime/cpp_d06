/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Base.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:39 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:40 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Base.class.hpp"

Base::Base() {}
Base::Base(Base const &obj) {
	*this = obj;
}
Base::~Base() {}

Base				&Base::operator=(Base const &obj) {
	static_cast<void>(obj);
	return (*this);
}
