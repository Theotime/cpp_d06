/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.class.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:42 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:44 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "C.class.hpp"

C::C() {}
C::C(C const &obj) {
	*this = obj;
}
C::~C() {}

C				&C::operator=(C const &obj) {
	static_cast<void>(obj);
	return (*this);
}
