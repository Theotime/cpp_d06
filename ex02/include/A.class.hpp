/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   A.class.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:51 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:52 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __A_CLASS_HPP__
# define __A_CLASS_HPP__

# include "Base.class.hpp"

class A : public Base {

	public:
		A();
		A(A const &obj);
		virtual ~A();

		A			&operator=(A const &obj);

};

#endif
