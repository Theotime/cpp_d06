/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.class.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:25:00 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:25:02 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __C_CLASS_HPP__
# define __C_CLASS_HPP__

# include "Base.class.hpp"

class C : public Base {

	public:
		C();
		C(C const &obj);
		virtual ~C();

		C				&operator=(C const &obj);
};

#endif
