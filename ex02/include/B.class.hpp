/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   B.class.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:53 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __B_CLASS_HPP__
# define __B_CLASS_HPP__

# include "Base.class.hpp"

class B : public Base {

	public:
		B();
		B(B const &obj);
		virtual ~B();

		B				&operator=(B const &obj);
};

#endif
