/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   identity.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:25:04 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:25:05 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __IDENTITY_HPP__
# define __IDENTITY_HPP__

# include <iostream>
# include <ctime>

# include "Base.class.hpp"
# include "A.class.hpp"
# include "B.class.hpp"
# include "C.class.hpp"

Base		*generate(void);

void		identify_from_pointer(Base *p);
void		identify_from_reference(Base &p);

#endif
