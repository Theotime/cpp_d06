/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Base.class.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:57 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:58 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __BASE_CLASS_HPP__
# define __BASE_CLASS_HPP__

class Base {

	public:
		Base();
		Base(Base const &obj);
		virtual ~Base();

		Base		&operator=(Base const &obj);

};

#endif
