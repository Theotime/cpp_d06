/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:01 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:04 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serialize.hpp"


int				main() {
	void		*raw = NULL;
	Data		*result = NULL;

	std::srand(std::time(0));
	raw = serialize();
	result = deserialize(raw);

	std::cout << "S1 : " << result->s1 << std::endl;
	std::cout << "N : " << result->n << std::endl;
	std::cout << "S2 : " << result->s2 << std::endl;

	delete result;
	result = deserialize(raw);
	std::cout << "	# RE DESERIALIZATION" << std::endl;
	std::cout << "S1 : " << result->s1 << std::endl;
	std::cout << "N : " << result->n << std::endl;
	std::cout << "S2 : " << result->s2 << std::endl;

	//delete raw;
	delete result;
	return (0);
}

static void		randomString(char *raw) {
	std::string			all = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	for (size_t i = 0; i < 8; ++i)
		raw[i] = all[std::rand() % all.length()];
}

void			*serialize(void) {
	void		*raw = static_cast<void*>(new Data());

	randomString(static_cast<char *>(raw));
	*reinterpret_cast<int *>((static_cast<char *>(raw) + 8)) = std::rand();
	randomString(static_cast<char *>(raw) + 8 + sizeof(int));
	return (raw);
}

Data			*deserialize(void *raw) {
	Data		*data = new Data;

	data->s1 = std::string(static_cast<char *>(raw), 8);
	data->n = *reinterpret_cast<int *>(static_cast<char *>(raw) + 8);
	data->s2 = std::string(static_cast<char *>(raw) + 8 + sizeof(int), 8);

	return data;
}
