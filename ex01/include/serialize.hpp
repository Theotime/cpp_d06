/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serialize.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:24:07 by triviere          #+#    #+#             */
/*   Updated: 2016/01/13 13:24:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __SERIALIZE_HPP__
# define __SERIALIZE_HPP__

# include <iostream>

struct Data {
	std::string		s1;
	int				n;
	std::string		s2;
};

void			*serialize(void);
Data			*deserialize(void *raw);


#endif
